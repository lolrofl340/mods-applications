# Mods Applications

## Introduction

This folder includes a few moderation practical tests written by /\\_ Unicorn _/\\

With these tests you can use the points system, take notes about each contestent and just generally see their way of operation and thinking.

### Setting up the test server

---

A template to the test server will be included, a custom notes bot was used at the time of using the server, which used for taking notes and generate scenarios.

Get the [template](https://discord.new/4waZaJjpCuQJ "Moderators Test Server")

