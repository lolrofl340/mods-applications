# Ping Chaos

In this scenario we have 2 members.

Member 2 asked for help from Member 1, but it didn't work as expected, and starts pinging Member 1 to help them, even after being asked to stop.

#### The goal: Testing the moderators reactions to pings

## Script

---

Member 1: Hello people

Member 2: Hello [Member 1]

Member 1: How are you doing?

Member 2: I'm fine, hbu?

Member 1: I'm doing great! thanks 😊

Member 2: Can you help me with something please?
What you've said yesterday doesn't seem to work 😕

**[ wait for a few minutes]**

Member 2: @[Member 1] hello? you there?

Member 1: Can you please not ping me? I'm in class right now...

Member 2: But it's gonna take just one sec....please?
I did everything you've said and still nothing seems to work

Member 2: @[Member 1] please help??

Member 1: **DON'T PING I AM IN CLASS**

Member 2: But I need your help

Member 1: Then wait until I'm done and don't ping me

Member 2: Ok....

---

about 15 minutes later

---

Member 2: @[Member 1] are you done yet?

Member 1: Could you please not ping me?

Member 1: @Moderator please help?

<details><summary>Expected Result</summary>
<pre>
Mods have warned Member 2 for pinging
</pre></details>

---

## Diagram

---

```mermaid
sequenceDiagram
participant Member 1
participant everyone
participant Member 2

Member 1 ->> everyone: Hello people
Member 2 -->> Member 1: Hello there
Member 1 ->> Member 2: What's up?
Member 2 -->>Member 1: I'm fine thanks, hbu?
Member 1 ->> Member 2: I'm doing gud, I'm in class rn so I'll ttyl later...
Member 2 -->> Member 1: Okie
Note over Member 1, Member 2: So far a typical convo

Note over Member 2, Member 1: 5 minutes later
Member 2 -->> Member 1: @[Member 1] I need your help
Member 1 ->> Member 2: Please don't ping me, I'm in class
Member 2 -->> Member 1: Sorry....
Note over Member 2, Member 1: Another 5 minutes later
Note over Member 2, Member 1: This is where stuff go bad
Member 2 -->> Member 1: @[Member 1] I really tried everything, <br/>it just doesn't work...<br/>how can I fix that?
Member 1 ->> Member 2: Please, don't @ me
Member 2 -->> Member 1: It's gonna be quick, really
Member 2 -->> Member 1: Please?
LOOP 5 ~ 10 minutes
    Member 2 -->> Member 1: @[Member 1] please?
end

Member 1 ->> everyone: @Moderator please help
```
